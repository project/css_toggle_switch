<?php

namespace Drupal\css_toggle_switch_webform\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\css_toggle_switch\Element\ToggleSwitch;
use Drupal\webform\Element\WebformEntityTrait;

/**
 * Provides a webform element for a entity radios.
 *
 * @FormElement("toggle_switch_entity")
 */
class ToggleSwitchEntity extends ToggleSwitch {

  use WebformEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function processToggleSwitch(&$element, FormStateInterface $form_state, &$complete_form) {
    static::setOptions($element);

    return parent::processToggleSwitch($element, $form_state, $complete_form);
  }

}

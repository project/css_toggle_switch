<?php

namespace Drupal\css_toggle_switch_webform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\OptionsBase;
use Drupal\webform\Plugin\WebformElement\Radios;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'toggle_switch' element.
 *
 * @WebformElement(
 *   id = "toggle_switch",
 *   label = @Translation("Toggle Switch"),
 *   description = @Translation("Provides a form element for a toggle switch."),
 *   category = @Translation("Options elements"),
 * )
 */
class ToggleSwitch extends Radios {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
      'toggle_on__attributes' => '',
      'toggle_type' => 'switch-toggle',
      'toggle_classes' => '',
      'wrapper_type' => 'container',
    ] + OptionsBase::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['options']['toggle_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Toggle type'),
      '#default_value' => $this->configuration['toggle_type'],
      '#required' => TRUE,
      '#options' => [
        'switch-light' => $this->t('Light'),
        'switch-toggle' => $this->t('Toggle'),
      ],
    ];
    $form['options']['toggle_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS classes to apply to the toggle switch'),
      '#default_value' => $this->configuration['toggle_classes'],
      '#description' => $this->t("Use a space to separate several classes. The CSS classes decide the behaviour of the element, see <a href='https://ghinda.net/css-toggle-switch/index.html'>the documentation</a> for all the options."
      ),
    ];
    $form['options']['toggle_on__attributes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Toggle ON Attributes'),
      '#description' => $this->t('Custom class to signal the selected option.'),
      '#default_value' => $this->configuration['toggle_on__attributes'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    $element['#type'] = 'radios';
    parent::prepare($element, $webform_submission);

    $element['#type'] = 'toggle_switch';
  }

  /**
   * {@inheritdoc}
   */
  protected function getElementInfoDefaultProperty(array $element, $property_name) {
    return $this->elementInfo->getInfoProperty('toggle_switch', $property_name, NULL);
  }

}

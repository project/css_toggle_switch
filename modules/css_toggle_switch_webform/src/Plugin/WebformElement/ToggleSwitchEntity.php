<?php

namespace Drupal\css_toggle_switch_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformEntityOptionsTrait;
use Drupal\webform\Plugin\WebformElement\WebformEntityReferenceTrait;
use Drupal\webform\Plugin\WebformElementEntityOptionsInterface;

/**
 * Provides a 'toggle_switch_entity' element.
 *
 * @WebformElement(
 *   id = "toggle_switch_entity",
 *   label = @Translation("Entity Toggle Switch"),
 *   description = @Translation("Provides a form element to select a single entity reference using the toggle switch element."),
 *   category = @Translation("Entity reference elements"),
 * )
 */
class ToggleSwitchEntity extends ToggleSwitch implements WebformElementEntityOptionsInterface {

  use WebformEntityReferenceTrait;
  use WebformEntityOptionsTrait;

}

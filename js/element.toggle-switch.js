/**
 * @file
 * JavaScript behaviors for Toggle Switch element integration.
 */

(function ($, Drupal) {

  'use strict'

  /**
   * Handle Toggle Switch touch events.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.cssToggleSwitch = {
    attach: function (context) {
      const elements = once('css-toggle-switch', '.css-toggle-switch', context)
      elements.forEach(
        function (element) {
          const $toggle = $(element)
          $toggle.on('swipeleft swiperight', function (event) {
            const $element = $(this)
            if (event.type === 'swipeleft') {
              $('input:checked', $element).prevAll(':radio:first').prop('checked', true)
            }
            else if (event.type === 'swiperight') {
              $('input:checked', $element).nextAll(':radio:first').prop('checked', true)
            }
          })
        }
      )
    }
  }

})(jQuery, Drupal)

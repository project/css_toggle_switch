<?php

namespace Drupal\css_toggle_switch\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\BetterExposedFiltersHelper;
use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * CSS Toggle Switch.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "css_toggle_switch",
 *   label = @Translation("CSS Toggle Switch"),
 * )
 */
class CssToggleSwitch extends FilterWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'toggle_type' => 'switch-toggle',
      'toggle_classes' => '',
      'toggle_on__attributes' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['toggle_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Toggle type'),
      '#default_value' => $this->configuration['toggle_type'],
      '#options' => [
        'switch-light' => $this->t('Light'),
        'switch-toggle' => $this->t('Toggle'),
      ],
    ];
    $form['toggle_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS classes to apply to the toggle switch'),
      '#default_value' => $this->configuration['toggle_classes'],
      '#description' => $this->t("Use a space to separate several classes. The CSS classes decide the behaviour of the element, see <a href='https://ghinda.net/css-toggle-switch/index.html'>the documentation</a> for all the options."
      ),
    ];
    $form['toggle_on__attributes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Toggle ON Attributes'),
      '#description' => $this->t('Custom class to signal the selected option.'),
      '#default_value' => $this->configuration['toggle_on__attributes'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    // Form element is designated by the element ID which is user-
    // configurable.
    $field_id = $filter->options['is_grouped'] ? $filter->options['group_info']['identifier'] : $filter->options['expose']['identifier'];

    parent::exposedFormAlter($form, $form_state);

    if (!empty($form[$field_id])) {
      // Clean up filters that pass objects as options instead of strings.
      if (!empty($form[$field_id]['#options'])) {
        $form[$field_id]['#options'] = BetterExposedFiltersHelper::flattenOptions($form[$field_id]['#options']);
      }
      $form[$field_id]['#type'] = 'toggle_switch';
      $form[$field_id]['#toggle_type'] = $this->configuration['toggle_type'];
      $form[$field_id]['#attributes']['class'][] = $this->configuration['toggle_classes'];
      $form[$field_id]['#toggle_on__attributes'] = $this->configuration['toggle_on__attributes'];
    }
  }

}

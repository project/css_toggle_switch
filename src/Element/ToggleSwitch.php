<?php

namespace Drupal\css_toggle_switch\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Radios;

/**
 * Provides a form element for the CSS Toggle Switch.
 *
 * This is based on the Radios element.
 *
 * Properties:
 * - '#toggle_type': Type of widget. Defaults to 'switch-light'.
 *
 *    @see https://ghinda.net/css-toggle-switch/index.html for additional
 *    documentation.
 *
 * Usage example:
 * @code
 * $form['settings']['active'] = [
 *   '#type' => 'toggle_switch',
 *   '#toggle_type' => 'switch-light',
 *   '#title' => $this->t('Poll status'),
 *   '#default_value' => 1,
 *   '#options' => [
 *     0 => $this->t('Closed'),
 *     1 => $this->t('Active'),
 *   ],
 *   '#attributes' => [
 *     'class' => ['switch-candy switch-candy-blue'],
 *   ],
 *   '#toggle_on__attributes' => [
 *     'class' => 'custom-class',
 *   ],
 * ];
 * @endcode
 *
 * @see https://ghinda.net/css-toggle-switch/index.html
 *
 * @FormElement("toggle_switch")
 */
class ToggleSwitch extends Radios {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    $info = parent::getInfo();
    $info['#process'][] = [$class, 'processToggleSwitch'];
    // Prevent nested fieldset by removing fieldset theme wrapper.
    // @see \Drupal\Core\Render\Element\CompositeFormElementTrait
    $info['#pre_render'] = [];
    $info['#theme_wrappers'] = ['toggle_switch'];

    return $info;
  }

  /**
   * Expands a toggle_switch element into individual option elements.
   */
  public static function processToggleSwitch(&$element, FormStateInterface $form_state, &$complete_form) {
    if (count($element['#options']) > 0) {
      foreach ($element['#options'] as $key => $choice) {
        $element[$key]['#type'] = 'toggle_switch_option';
        // Don't propagate element attributes.
        unset($element[$key]['#attributes']);
      }
    }

    $element['#attached']['library'][] = 'css_toggle_switch/element.toggle_switch';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Always use a default value.
    if ($input === FALSE && !isset($element['#default_value']) && !empty($element['#options'])) {
      reset($element['#options']);
      $element['#default_value'] = key($element['#options']);
    }

    if ($input !== FALSE) {
      // When there's user input (including NULL), return it as the value.
      // However, if NULL is submitted, FormBuilder::handleInputElement() will
      // apply the default value, and we want that validated against #options
      // unless it's empty. (An empty #default_value, such as NULL or FALSE, can
      // be used to indicate that no radio button is selected by default.)
      if (!isset($input) && !empty($element['#default_value'])) {
        $element['#needs_validation'] = TRUE;
      }
      return $input;
    }
    elseif (!empty($element['#default_value'])) {
      return $element['#default_value'];
    }
    else {
      return NULL;
    }
  }

}

<?php

namespace Drupal\css_toggle_switch\Element;

use Drupal\Core\Render\Element\Radio;

/**
 * Provides a form element for a single toggle_switch_option.
 *
 * @FormElement("toggle_switch_option")
 */
class ToggleSwitchOption extends Radio {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#theme_wrappers'] = ['toggle_switch_option'];

    return $info;
  }

}
